// const bodyParser = require('body-parser')
const express = require('express')

const app = express()
const PORT = 3000
const Data = require("./data.js")

app.use(express.urlencoded({ extended: true })) // app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.json()) // app.use(bodyParser.json())

// curl "http://127.0.0.1:3000/toys
app.get("/toys", function(requête, réponse) {
    // réponse.send(Data) // #=> Client seulement (Data complétement visible)
    // réponse.send(Data.toys) // #=> Client seulement
    //! réponse.send("client =>", Data.toys) // #=> Client & Serveur
    //! réponse.send(Data.toys ,{message: "client =>"}) // #=> Client & Serveur
    //! réponse.send("client =>", {data: Data.toys}) // #=> Client & Serveur
    // réponse.send(Data.categories) // #=> Client seulement
    // réponse.send(`Client Data => ${Data}`) // #=> Client seulement [object Object]
    // réponse.send(`Client Data => ${Object.keys(Data)}`) // #=> Client seulement toys,categories
    // réponse.send(`Client Toys => ${Object.keys(Data.toys)}`) // #=> Client seulement | nombre d'éléement dans toys sous forme d'index; ex: 0,1,2,3
    // réponse.send(`Client Toys => ${Object.keys(Data.toys[0])}`) // #=> Client seulement | nombre d'éléement dans toys sous forme d'index; ex: 0,1,2,3
    // réponse.send(`Client Categories => ${Object.keys(Data.categories)}`) // #=> Client seulement | nombre d'éléement dans categories
    // console.log(Data) // #=> Server seulement | Data structuré
    // console.log(Data.toys) // #=> Server seulement | Data.toys structuré
    // console.log(Data.categories) // #=> Server seulement | Data.categories structuré
    // console.log("Server =>", Data) // #=> Server seulement | Data structuré
    console.log("Server =>", Data.toys) // #=> Server seulement | Data.toys structuré
    // console.log("Server =>", Data.categories) // #=> Server seulement | Data.toys structuré
})

app.get("/toys/:id", function(requête, réponse) {
    if(requête.params.id < Data.toys.length){
        let id = requête.params.id
        réponse.send(Data.toys[id])
        console.log("#Server#=>", Data.toys[id]) // #=> Server seulement | Data.toys structuré
    }else{ réponse.status(404).send("Not Found"); console.log("Not Found") }
})

// curl -d "name=Minesweeper&description=Home computer classic&price=0&category_id=5" -X POST http://127.0.0.1:3000/toys
// curl -X POST http://127.0.0.1:3000/toys?name=chrissMcKenzie&description=Développeur Web&price=250&category_id=7"
// Exo Create
// curl -X POST http://127.0.0.1:3000/toys?name=chrissMcKenzie&description=DeveloppeurWeb&price=250&category_id=5"
app.post("/toys", function(requête, réponse) {
    if(requête.query.name && requête.query.description && requête.query.price && requête.query.category_id){
        requête.query.price = parseInt(requête.query.price)
        requête.query.category_id = parseInt(requête.query.category_id)
        Data.toys.push(requête.query)
        console.log(Data.toys[Data.toys.length - 1]); réponse.send(Data.toys[Data.toys.length - 1])
    }else{ réponse.status(422).send("Unprocessable Entity"); console.log("Unprocessable Entity") }
})

app.put("/toys/:id", function(requête, réponse) {
    if(Object.keys(requête.query).length <= 0){
        if(requête.params.id < Data.toys.length){
            let id = parseInt(requête.params.id) // console.log(requête.body);
            réponse.send(Data.toys[id])
            console.log(Data.toys[id])
        } else{ réponse.status(404).send("Not Found"); console.log("Not Found") }

    }else{
        if(requête.params.id < Data.toys.length){
            let id = parseInt(requête.params.id)
            requête.query.price = parseInt(requête.query.price)
            requête.query.category_id = parseInt(requête.query.category_id)

            Data.toys[id].name = requête.query.name ? requête.query.name : Data.toys[id].name
            Data.toys[id].description = requête.query.description ? requête.query.description : Data.toys[id].description
            Data.toys[id].price = requête.query.price ? requête.query.price : Data.toys[id].price
            Data.toys[id].category_id = requête.query.category_id ? requête.query.category_id : Data.toys[id].category_id
        
            console.log(Data.toys[id])
        } else{ réponse.status(404).send("Not Found"); console.log("Not Found") }
    }
})

app.delete("/toys/:id", function(requête, réponse) {
    if(requête.params.id < Data.toys.length){
        let id = parseInt(requête.params.id)
        let deletedToy = Data.toys.splice(id, 1)
        console.log(deletedToy); réponse.send(deletedToy)
    } else{ réponse.status(404).send("Not Found"); console.log("Not Found") }
})


app.get("/categories", function(requête, réponse) {
    console.log("Server =>", Data.categories) // #=> Server seulement | Data.toys structuré
})

app.get("/categories/:id", function(requête, réponse) {
    if(requête.params.id < Data.categories.length){
        let id = requête.params.id
        réponse.send(Data.categories[id])
        console.log("#Server#=>", Data.categories[id]) // #=> Server seulement | Data.categories structuré
    }else{ réponse.status(404).send("Not Found"); console.log("Not Found") }
})

// curl -d "name=Minesweeper&description=Home computer classic&price=0&category_id=5" -X POST http://127.0.0.1:3000/categories
// curl -X POST http://127.0.0.1:3000/categories?name=chrissMcKenzie&description=Développeur Web&price=250&category_id=7"
// Exo Create
// curl -X POST http://127.0.0.1:3000/categories?name=chrissMcKenzie&description=DeveloppeurWeb&price=250&category_id=5"
app.post("/categories", function(requête, réponse) {
    if(requête.query.name && requête.query.description && requête.query.price && requête.query.category_id){
        requête.query.price = parseInt(requête.query.price)
        requête.query.category_id = parseInt(requête.query.category_id)
        Data.categories.push(requête.query)
        console.log(Data.categories[Data.categories.length - 1]); réponse.send(Data.categories[Data.categories.length - 1])
    }else{ réponse.status(422).send("Unprocessable Entity"); console.log("Unprocessable Entity") }
})

app.put("/categories/:id", function(requête, réponse) {
    if(Object.keys(requête.query).length <= 0){
        if(requête.params.id < Data.categories.length){
            let id = parseInt(requête.params.id) // console.log(requête.body);
            réponse.send(Data.categories[id])
            console.log(Data.categories[id])
        } else{ réponse.status(404).send("Not Found"); console.log("Not Found") }

    }else{
        if(requête.params.id < Data.categories.length){
            let id = parseInt(requête.params.id)
            requête.query.price = parseInt(requête.query.price)
            requête.query.category_id = parseInt(requête.query.category_id)

            Data.categories[id].name = requête.query.name ? requête.query.name : Data.categories[id].name
            Data.categories[id].description = requête.query.description ? requête.query.description : Data.categories[id].description
            Data.categories[id].price = requête.query.price ? requête.query.price : Data.categories[id].price
            Data.categories[id].category_id = requête.query.category_id ? requête.query.category_id : Data.categories[id].category_id
        
            console.log(Data.categories[id])
        } else{ réponse.status(404).send("Not Found"); console.log("Not Found") }
    }
})

app.delete("/categories/:id", function(requête, réponse) {
    if(requête.params.id < Data.categories.length){
        let id = parseInt(requête.params.id)
        let deletedToy = Data.categories.splice(id, 1)
        console.log(deletedToy); réponse.send(deletedToy)
    } else{ réponse.status(404).send("Not Found"); console.log("Not Found") }
})

app.listen(PORT, ()=> console.log(`Écoute du port ${PORT}`))