const express = require('express')
const app = express()

app.get("/toys/:name", function(requête, réponse) {
    if(requête.params.name === "unknown"){
            réponse.status(404).send("Not Found\n")
        }else{
            réponse.send(`I am the toy ${requête.params.name}\n`)
        }
    }
).listen(3000)

// exports.post_new_toy = function(requête, réponse) {
//     if(requête.params.name === "unknown"){
//         réponse.status(404).send("Not Found\n")
//     }else{
//         réponse.send(``)
//     }
// }