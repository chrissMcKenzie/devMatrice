const express = require('express')
const bodyParser = require('body-parser')
const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get("/toys", function(requête, réponse) {
    if(Object.keys(requête.query).length === 0){
        réponse.send("Get all the toys\n")
    }else{
        console.log(requête.query) // console.log(requête.query.owner)
    }    
}).listen(3000)



// exports.post_new_toy = function(requête, réponse) {
//     réponse.send("Hello Santa!")
// }