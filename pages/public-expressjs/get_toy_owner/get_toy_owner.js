const express = require('express')
const app = express()

app.get("/toys/:name/:owner", function(requête, réponse) {
    if(requête.params.name === "unknown"){
        réponse.status(404).send("Not Found\n")
    }else{
        réponse.send(`I don't know who my ${requête.params.owner} is\n`)
    }
}).listen(3000)


// exports.post_new_toy = function(requête, réponse) {
//     réponse.send("Hello Santa!")
// }