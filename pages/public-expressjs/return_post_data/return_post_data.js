const express = require('express')
const bodyParser = require('body-parser')
const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.post("/toys/:name/owner", function(requête, réponse) {
    if(Object.keys(requête.query).length === 0){
        réponse.send(`I added a new owner!\n`)
    }else{
        console.log(requête.query) // console.log(requête.query.owner)
    }
})

app.get("/toys/:name/owner", function(requête, réponse) {
    console.log(requête.query) // console.log(requête.query.owner)
})

app.listen(3000)
// exports.return_post_data = function(requête, réponse) {
//     réponse.send("Hello Santa!")
// }