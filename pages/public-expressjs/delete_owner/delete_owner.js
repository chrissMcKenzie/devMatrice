const express = require('express')
const app = express()

app.delete("/toys/:name/:owner", function(requête, réponse) {
    réponse.send(`I deleted the owner!\n`)
})

app.get("/toys/:name/owner", function(requête, réponse) {
    if(requête.params.name === "unknown"){
        réponse.status(404).send("Not Found\n")
    }else{
        réponse.send(`I don't know who my owner is\n`)
    }
})

app.listen(3000, ()=> console.log("Test"))
// exports.post_new_toy = function(requête, réponse) {
//     réponse.send("Hello Santa!")
//}