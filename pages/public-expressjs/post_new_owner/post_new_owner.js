const express = require('express')
const app = express()

app.post("/toys/:name/:owner", function(requête, réponse) {
    réponse.send(`I added a new owner!\n`)
})

app.get("/toys/:name/owner", function(requête, réponse) {
    if(requête.params.name === "unknown"){
        réponse.status(404).send("Not Found\n")
    }else{
        réponse.send(`I don't know who my owner is\n`)
    }
})

app.listen(3000)