//pas secure du tout !
const OCD_API_KEY= "37035140d16941a69a2d2cbda9401ba2"
const OWM_API_KEY= "d0fcc00c02efe5b8355fa57156f79f2b"

function getDataFromOCD(ville){
    let URL = `https://api.opencagedata.com/geocode/v1/json?q=${ville}&key=${OCD_API_KEY}&pretty=1`

    return fetch(URL)
            .then(res => res.json())
            .then(data => data.results[0])
            .catch(err => console.log(err))
}

function getDataFromOWM(coords){
    let URL2 = `https://api.openweathermap.org/data/2.5/onecall?lat=${coords.lat}&lon=${coords.long}&exclude={part}&appid=${OWM_API_KEY}`

    return fetch(URL2)
            .then(res => res.json())
            .catch(err => console.log(err))

}

// let coords ={
//     lat:48.8588897,
//     long : 2.320041
// }



document.getElementById('form-meteo').addEventListener('submit',(event)=>{
    event.preventDefault()

    let inputCityName = document.getElementById('city').value.toLowerCase()
    console.log(inputCityName)

    getDataFromOCD(inputCityName)
    .then(rawData => {
        console.log('rawdata',rawData)

        let coordsData = {
            lat : rawData.geometry.lat,
            long : rawData.geometry.lng
        }

        getDataFromOWM(coordsData)
            .then(response => console.log('reponse',response))
    })


})