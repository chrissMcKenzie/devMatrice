import { APIKEY_OpenCage, APIKEY_OpenWeather } from "./../Vars.js";

async function FetcherOpenCage(ville){
    let URL = `https://api.opencagedata.com/geocode/v1/json?q=${ville}&pretty=1&key=${APIKEY_OpenCage}`
    return await fetch(URL)
        .then((réponse) => réponse.json())
        .then((data) => {
            console.log("DataApiOpenCage", data);
            let newDataFromOpenCage = {
                ville: data.results[0].formatted,
                longitude: data.results[0].geometry.lng,
                latitude: data.results[0].geometry.lat,
                date: data.timestamp.created_http,
            }
            // console.log("newDataFromOpenCage", newDataFromOpenCage);
            return newDataFromOpenCage
        })
        .catch((erreur)=> erreur)
}

async function FetcherOpenWeather(latitude, longitude){
    let URL = `https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&appid=${APIKEY_OpenWeather}`
    return await fetch(URL)
        .then((réponse)=> réponse.json())
        .then((data) => {
            console.log("DataApiOpenWeather", data);
            let newDataFromOpenWeather = {
                jours: data.daily,
                nuageux: data.daily
            }
            // console.log("newDataFromOpenWeather", newDataFromOpenWeather);
            return newDataFromOpenWeather
        })
        .catch((erreur) => erreur)
}

async function FetcherSunriseSunset(latitude, longitude){
    let URL = `https://api.sunrise-sunset.org/json?lat=${latitude}&lng=${longitude}`
    return await fetch(URL)
        .then((réponse)=> réponse.json())
        .then((data) => {
            console.log("DataApiSunriseSunset", data);
            // let newDataFromSunriseSunset = {
                
            // }
            // return newDataFromSunriseSunset
        })
        .catch((erreur) => erreur)
}

export {FetcherOpenCage, FetcherOpenWeather, FetcherSunriseSunset}