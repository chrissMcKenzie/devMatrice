import { FetcherOpenCage, FetcherOpenWeather, FetcherSunriseSunset } from "./modules/functions/ApiFetcher.js"

// https://www.gradientmagic.com/collection/simplecircles
ButtonOpenCage.onclick = async()=>{
    OutPut.innerHTML = ""
    FetcherOpenCage(Input.value)
        .then(savedDataFromOpenCage => {
            // console.log("data", savedDataFromOpenCage)
            FetcherOpenWeather(savedDataFromOpenCage.latitude, savedDataFromOpenCage.longitude)
                .then(savedDataFromOpenWeather => {
                        // console.log("savedDataFromOpenWeather", savedDataFromOpenWeather)
                        let Jours = savedDataFromOpenWeather.jours.map(i => new Date(parseInt(i.dt * 1000)).getDay())
                        // console.log("Jours", Jours) // WeekDaySpan(weekTab(today + iDay) % 7)
                        let Lundi = Jours[0]
                        let Mardi = Jours[1]
                        let Mercredi = Jours[2]
                        let Jeudi = Jours[3]
                        let Vendredi = Jours[4]
                        let Samedi = Jours[5]
                        let Dimanche = Jours[6]
                        let Météos = savedDataFromOpenWeather.nuageux.map(i => i.clouds)
                        // console.log("Météos", Météos)
                        console.log("JoursAfficher", JoursAfficher.value)

                        switch(JoursAfficher.value){
                            case "1":
                                OutPut.innerHTML += `
                                    <output>
                                        <h3>${Lundi == 1 ? "Lundi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[0] == 0 ? "sun" : Météos[0] > 50 ? "clouds" : Météos[0] > 0 && Météos[0] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                `
                                break;
                            case "2":
                                OutPut.innerHTML += `
                                    <output>
                                        <h3>${Lundi == 1 ? "Lundi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[0] == 0 ? "sun" : Météos[0] > 50 ? "clouds" : Météos[0] > 0 && Météos[0] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Mardi == 2 ? "Mardi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[1] == 0 ? "sun" : Météos[1] > 50 ? "clouds" : Météos[1] > 0 && Météos[1] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                `
                                break;
                            case "3":
                                OutPut.innerHTML += `
                                    <output>
                                        <h3>${Lundi == 1 ? "Lundi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[0] == 0 ? "sun" : Météos[0] > 50 ? "clouds" : Météos[0] > 0 && Météos[0] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Mardi == 2 ? "Mardi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[1] == 0 ? "sun" : Météos[1] > 50 ? "clouds" : Météos[1] > 0 && Météos[1] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Mercredi == 3 ? "Mercredi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[2] == 0 ? "sun" : Météos[2] > 50 ? "clouds" : Météos[2] > 0 && Météos[2] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                `
                                break;
                            case "4":
                                OutPut.innerHTML += `
                                    <output>
                                        <h3>${Lundi == 1 ? "Lundi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[0] == 0 ? "sun" : Météos[0] > 50 ? "clouds" : Météos[0] > 0 && Météos[0] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Mardi == 2 ? "Mardi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[1] == 0 ? "sun" : Météos[1] > 50 ? "clouds" : Météos[1] > 0 && Météos[1] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Mercredi == 3 ? "Mercredi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[2] == 0 ? "sun" : Météos[2] > 50 ? "clouds" : Météos[2] > 0 && Météos[2] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Jeudi == 4 ? "Jeudi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[3] == 0 ? "sun" : Météos[3] > 50 ? "clouds" : Météos[3] > 0 && Météos[3] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                `
                                break;
                            case "5":
                                OutPut.innerHTML += `
                                    <output>
                                        <h3>${Lundi == 1 ? "Lundi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[0] == 0 ? "sun" : Météos[0] > 50 ? "clouds" : Météos[0] > 0 && Météos[0] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Mardi == 2 ? "Mardi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[1] == 0 ? "sun" : Météos[1] > 50 ? "clouds" : Météos[1] > 0 && Météos[1] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Mercredi == 3 ? "Mercredi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[2] == 0 ? "sun" : Météos[2] > 50 ? "clouds" : Météos[2] > 0 && Météos[2] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Jeudi == 4 ? "Jeudi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[3] == 0 ? "sun" : Météos[3] > 50 ? "clouds" : Météos[3] > 0 && Météos[3] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Vendredi == 5 ? "Vendredi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[4] == 0 ? "sun" : Météos[4] > 50 ? "clouds" : Météos[4] > 0 && Météos[4] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                `
                                break;
                            case "6":
                                OutPut.innerHTML += `
                                    <output>
                                        <h3>${Lundi == 1 ? "Lundi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[0] == 0 ? "sun" : Météos[0] > 50 ? "clouds" : Météos[0] > 0 && Météos[0] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Mardi == 2 ? "Mardi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[1] == 0 ? "sun" : Météos[1] > 50 ? "clouds" : Météos[1] > 0 && Météos[1] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Mercredi == 3 ? "Mercredi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[2] == 0 ? "sun" : Météos[2] > 50 ? "clouds" : Météos[2] > 0 && Météos[2] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Jeudi == 4 ? "Jeudi" : "" }</h3>
                                        <img src="./../media/images/weathers/${Météos[3] == 0 ? "sun" : Météos[3] > 50 ? "clouds" : Météos[3] > 0 && Météos[3] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Vendredi == 5 ? "Vendredi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[4] == 0 ? "sun" : Météos[4] > 50 ? "clouds" : Météos[4] > 0 && Météos[4] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Samedi == 6 ? "Samedi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[5] == 0 ? "sun" : Météos[5] > 50 ? "clouds" : Météos[5] > 0 && Météos[5] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                `
                                break;
                            case "7":
                                OutPut.innerHTML += `
                                    <output>
                                        <h3>${Lundi == 1 ? "Lundi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[0] == 0 ? "sun" : Météos[0] > 50 ? "clouds" : Météos[0] > 0 && Météos[0] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Mardi == 2 ? "Mardi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[1] == 0 ? "sun" : Météos[1] > 50 ? "clouds" : Météos[1] > 0 && Météos[1] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Mercredi == 3 ? "Mercredi" : "" }</h3>
                                        <img src="./../media/images/weathers/${Météos[2] == 0 ? "sun" : Météos[2] > 50 ? "clouds" : Météos[2] > 0 && Météos[2] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Jeudi == 4 ? "Jeudi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[3] == 0 ? "sun" : Météos[3] > 50 ? "clouds" : Météos[3] > 0 && Météos[3] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Vendredi == 5 ? "Vendredi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[4] == 0 ? "sun" : Météos[4] > 50 ? "clouds" : Météos[4] > 0 && Météos[4] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Samedi == 6 ? "Samedi" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[5] == 0 ? "sun" : Météos[5] > 50 ? "clouds" : Météos[5] > 0 && Météos[5] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                    <output>
                                        <h3>${Dimanche == 0 ? "Dimanche" : "" }</h3>
                                        <img src="./../../media/images/weathers/${Météos[6] == 0 ? "sun" : Météos[6] > 50 ? "clouds" : Météos[6] > 0 && Météos[6] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                        <h4>${savedDataFromOpenCage.ville}</h4>
                                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                    </output>
                                `
                                break;

                            default:
                                break;
                        }
                        
                })

            FetcherSunriseSunset(savedDataFromOpenCage.latitude, savedDataFromOpenCage.longitude)
    })

}