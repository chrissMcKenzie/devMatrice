const APIKEY_OpenCage = "37035140d16941a69a2d2cbda9401ba2"
const APIKEY_OpenWeather = "d0fcc00c02efe5b8355fa57156f79f2b"

async function FetcherOpenCage(ville){
    let URL = `https://api.opencagedata.com/geocode/v1/json?q=${ville}&pretty=1&key=${APIKEY_OpenCage}`
    return await fetch(URL)
        .then((réponse) => réponse.json())
        .then((data) => {
            console.log("OpenCage", data);
            //console.log(data.results[0].formatted); console.log(data.results[0].geometry.lng); console.log(data.results[0].geometry.lat); console.log(data.timestamp.created_http)
            let newDataFromOpenCage = {
                ville: data.results[0].formatted,
                longitude: data.results[0].geometry.lng,
                latitude: data.results[0].geometry.lat,
                date: data.timestamp.created_http,
            }
            return newDataFromOpenCage
        })
        .catch((erreur)=> erreur)
}

async function FetcherOpenWeather(latitude, longitude){
    let URL = `https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&appid=${APIKEY_OpenWeather}`
    return await fetch(URL)
        .then((réponse)=> réponse.json())
        .then((data) => {
            console.log("OpenWeather", data); //console.log(data.coord); console.log(data.clouds.all); console.log(data.weather[0].main)
            // console.log("Cloudly", data.daily.map(i => i.clouds));
            let newDataFromOpenWeather = {
                jours: data.daily,
                nuageux: data.daily
            }
            return newDataFromOpenWeather
        })
        .catch((erreur) => erreur)
    }
    
    ButtonOpenCage.onclick = async()=>{
        OutPut.innerHTML = ""
        FetcherOpenCage(Input.value)
            .then(savedDataFromOpenCage => {
                // console.log("data", savedDataFromOpenCage)
                FetcherOpenWeather(savedDataFromOpenCage.latitude, savedDataFromOpenCage.longitude)
                    .then(savedDataFromOpenWeather => {
                            // console.log("savedDataFromOpenWeather", savedDataFromOpenWeather)
                            let Jours = savedDataFromOpenWeather.jours.map(i => new Date(parseInt(i.dt * 1000)).getDay())
                            // console.log("Jours", Jours)
                            let Lundi = Jours[0]
                            let Mardi = Jours[1]
                            let Mercredi = Jours[2]
                            let Jeudi = Jours[3]
                            let Vendredi = Jours[4]
                            let Samedi = Jours[5]
                            let Dimanche = Jours[6]
                            let Météos = savedDataFromOpenWeather.nuageux.map(i => i.clouds)
                            console.log("Météos", Météos)

                            OutPut.innerHTML += `
                                <output>
                                    <h3>${Lundi == 1 ? "Lundi" : "" }</h3>
                                    <img src="./../media/images/weathers/${Météos[0] == 0 ? "sun" : Météos[0] > 50 ? "clouds" : Météos[0] > 0 && Météos[0] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                    <h4>${savedDataFromOpenCage.ville}</h4>
                                    <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                </output>
                                <output>
                                    <h3>${Mardi == 2 ? "Mardi" : "" }</h3>
                                    <img src="./../media/images/weathers/${Météos[1] == 0 ? "sun" : Météos[1] > 50 ? "clouds" : Météos[1] > 0 && Météos[1] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                    <h4>${savedDataFromOpenCage.ville}</h4>
                                    <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                </output>
                                <output>
                                    <h3>${Mercredi == 3 ? "Mercredi" : "" }</h3>
                                    <img src="./../media/images/weathers/${Météos[2] == 0 ? "sun" : Météos[2] > 50 ? "clouds" : Météos[2] > 0 && Météos[2] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                    <h4>${savedDataFromOpenCage.ville}</h4>
                                    <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                </output>
                                <output>
                                    <h3>${Jeudi == 4 ? "Jeudi" : "" }</h3>
                                    <img src="./../media/images/weathers/${Météos[3] == 0 ? "sun" : Météos[3] > 50 ? "clouds" : Météos[3] > 0 && Météos[3] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                    <h4>${savedDataFromOpenCage.ville}</h4>
                                    <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                </output>
                                <output>
                                    <h3>${Vendredi == 5 ? "Vendredi" : "" }</h3>
                                    <img src="./../media/images/weathers/${Météos[4] == 0 ? "sun" : Météos[4] > 50 ? "clouds" : Météos[4] > 0 && Météos[4] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                    <h4>${savedDataFromOpenCage.ville}</h4>
                                    <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                </output>
                                <output>
                                    <h3>${Samedi == 6 ? "Samedi" : "" }</h3>
                                    <img src="./../media/images/weathers/${Météos[5] == 0 ? "sun" : Météos[5] > 50 ? "clouds" : Météos[5] > 0 && Météos[5] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                    <h4>${savedDataFromOpenCage.ville}</h4>
                                    <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                </output>
                                <output>
                                    <h3>${Dimanche == 0 ? "Dimanche" : "" }</h3>
                                    <img src="./../media/images/weathers/${Météos[6] == 0 ? "sun" : Météos[6] > 50 ? "clouds" : Météos[6] > 0 && Météos[6] <= 50 ? "cloudy" : "rain"}.svg" width="40%" heigth="30%" alt="icon ${Météos} en svg">
                                    <h4>${savedDataFromOpenCage.ville}</h4>
                                    <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                                </output>
                            `
                    })
        })
}