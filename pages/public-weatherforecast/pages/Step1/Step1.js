const APIKEY_OpenCage = "d465d2ef1cce488291db0224a92337fd"
const APIKEY_OpenWeather = "3b354463e70f4ca60aa55521d3482714"

async function FetcherOpenCage(ville){
    let URL = `https://api.opencagedata.com/geocode/v1/json?q=${ville}&pretty=1&key=${APIKEY_OpenCage}`
    return await fetch(URL)
        .then((réponse) => réponse.json())
        .then((data) => {
            console.log(data); 
            //console.log(data.results[0].formatted); console.log(data.results[0].geometry.lng); console.log(data.results[0].geometry.lat); console.log(data.timestamp.created_http)
            let newDataFromOpenCage = {
                ville: data.results[0].formatted,
                longitude: data.results[0].geometry.lng,
                latitude: data.results[0].geometry.lat,
                date: data.timestamp.created_http,
            }
            return newDataFromOpenCage
        })
        .catch((erreur)=> erreur)
}

async function FetcherOpenWeather(latitude, longitude){
    let URL = `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${APIKEY_OpenWeather}`
    return await fetch(URL)
        .then((réponse)=> réponse.json())
        .then((data) => {
            console.log(data); //console.log(data.coord); console.log(data.clouds.all); console.log(data.weather[0].main)
            let newDataFromOpenWeather = {
                nom: data.name,
                coordonées: data.coord,
                nuage: data.clouds.all,
                weather: data.weather[0].main
            }
            return newDataFromOpenWeather
        })
        .catch((erreur)=> erreur)
}

ButtonOpenCage.onclick = async()=>{
    FetcherOpenCage(Input.value)
        .then(savedDataFromOpenCage => {
            console.log("data", savedDataFromOpenCage)
            FetcherOpenWeather(savedDataFromOpenCage.latitude, savedDataFromOpenCage.longitude)
                .then(savedDataFromOpenWeather => {
                    let Jour = new Date(savedDataFromOpenCage.date)
                    let Metéo = savedDataFromOpenWeather.weather == "Clear" ? "sun" : savedDataFromOpenWeather.nuage >= 50 ? "clouds" : savedDataFromOpenWeather.nuage <= 50 && savedDataFromOpenWeather.nuage >= 0 ? "cloudy" : "rain"
                    OutPut.innerHTML = `
                        <h2>${Jour.getDay() == 1 ? "Lundi" : Jour.getDay() == 2 ? "Mardi" : Jour.getDay() == 3 ? "Mercredi" : Jour.getDay() == 4 ? "Jeudi" : Jour.getDay() == 5 ? "Vendredi" : Jour.getDay() == 6 ? "Samedi" : Jour.getDay() == 7 ? "Dimanche" : "" }</h2>
                        <img src="./../media/images/weathers/${Metéo}.svg" width="50%" heigth="40%" alt="icon ${Metéo} en svg">
                        <h3>${savedDataFromOpenCage.ville}</h3>
                        <p><small>longitude: ${savedDataFromOpenCage.latitude}, latitude: ${savedDataFromOpenCage.longitude}</small></p>
                    `
                })
        })
}