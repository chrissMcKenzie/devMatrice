function AppMain() {
  return `
    <main>Main
      <section>
        <h1>IA by CMK</h1>
        <span style="display: inline-block;">
          <p>Tapez une phrase ! ou poser une question</p>
          <input type="text" id="phrase" name="phrase" placeholder="phrase" required>
          <button onclick="new App().Envoyer()">Envoyer</button>
        </span>
        
        <span style="display: inline-block;">
          <h2>Dictionnaire</h2>
          <div>
            <ul id="Dictionnaire">
              ${this.dictionnaire.map(i => i.replaceAll(",", ", ") )}
            </ul>
          </div>
        </span>
      </section>
    </main>
  `
}

module.exports = AppMain