const insertDouble = require('../src/insertDouble.js');

describe("insertDouble tests", () => {
 test('capitalized snake_case to camelCase', () => {
   expect(insertDouble("Hello_my_name_is_bond")).toBe("HelloMyNameIsBond");
 });


  test('empty string', () => {
   expect(insertDouble("")).toBe("");
 });
})