const toCamelCase = require('../src/toCamelCase');

describe("toCamelCase tests", () => {
 test('capitalized snake_case to camelCase', () => {
   expect(toCamelCase("Hello_my_name_is_bond")).toBe("HelloMyNameIsBond");
 });

 test('snake_case with mixed cases to camelCase', () => {
   expect(toCamelCase("HeLlo_PyThON")).toBe("HelloPython");
 });

 test('lowercase snake_case to camelCase', () => {
   expect(toCamelCase("oh_my_god")).toBe("ohMyGod");
 });

  test('empty string', () => {
   expect(toCamelCase("")).toBe("");
 });
})