function toCamelCase(string) {
    let newStringCapitalized = ""
    let newStringCamelized = ""
    if(string === ""){
        return ""
    }else{
        const arrayString = string.split("_")
            arrayString.map(element => {
                const firstLetter = element[0].charAt(0).toUpperCase()
                const lastLetters = element.slice(1).toLowerCase()
                newWordCamelized = firstLetter + lastLetters
                newStringCapitalized += newWordCamelized
            });
    }
    newStringCamelized = newStringCapitalized.charAt(0).toLowerCase() + newStringCapitalized.slice(1)
    return string[0] === string[0].toLowerCase() ? newStringCamelized : newStringCapitalized
}

toCamelCase("Gello_my_name_is_bond")
console.log(toCamelCase("Gello_my_name_is_bond"))
toCamelCase("gello_my_name_is_bond")
console.log(toCamelCase("gello_my_name_is_bond"))

module.exports = toCamelCase;



